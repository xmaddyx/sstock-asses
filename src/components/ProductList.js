import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Panel, Media, Jumbotron, Button, Image } from 'react-bootstrap'
import fb from '../firebase'
import DressAPI from '../api'

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dresses: []
        }
    }

    componentDidMount() {
        const ordersRef = fb.database().ref('books');


        ordersRef.once('value', snapshot => {
            let count = 0;
            DressAPI.players = [];
            snapshot.forEach((child, wer) => {
                DressAPI.players.push({
                    number: count,
                    name: child.val().title,
                    price: child.val().price
                })
                this.setState({
                    dresses: this.state.dresses.concat(child.val())
                })
                count++;
            })
        })
    }

    render() {
        console.log(DressAPI.players);
        return (
            <div className="container">
                <Jumbotron>
                    <h1>Dress</h1>
                    <p>Koleksi dress cantik sesuai gaya Sista dimanapun</p>
                </Jumbotron>

                {this.state.dresses.map((item, index) => (
                    <Panel key={index}>
                        <Panel.Body>
                            <Media>
                                <Media.Left>
                                    <Image width={300} height={450} src="https://dummyimage.com/300x450"/>
                                </Media.Left>
                                <Media.Body>
                                    <Media.Heading bsClass="text-left">
                                        <Link to={'/img/' + index}>{item.title}</Link>
                                    </Media.Heading>
                                    <p className="text-justify">
                                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                                        ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at,
                                        tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate
                                        fringilla. Donec lacinia congue felis in faucibus.
                                    </p>
                                    <br/>
                                    <p><b>IDR {item.price}</b></p>
                                </Media.Body>
                            </Media>
                        </Panel.Body>
                        <Panel.Footer>
                            <div className="text-right">
                                <Button bsStyle="danger">Beli</Button>
                            </div>
                        </Panel.Footer>
                    </Panel>
                ))}
            </div>
        )
    }
}

export default ProductList
