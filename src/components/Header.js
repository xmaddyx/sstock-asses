import React from 'react'
import { Link } from 'react-router-dom'
import { Navbar } from 'react-bootstrap'

const Header = () => (
    <Navbar staticTop>
      <Navbar.Header>
          <Navbar.Brand>
              <Link to='/'>Sale Stock</Link>
          </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
)

export default Header
