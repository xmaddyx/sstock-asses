import React, { Component } from 'react'
import { Panel, Image, Alert } from 'react-bootstrap'
import DressAPI from '../api'

class ProductDetail extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const dress = DressAPI.get(
            parseInt(this.props.match.params.id, 10)
        )
        if (!dress) {
            return <div className="container"><Alert bsStyle="warning">Sorry, but the dress was not found</Alert></div>
        }
        return (
            <div className="container">
                <Panel>
                    <Panel.Heading>{dress.name}</Panel.Heading>
                    <Panel.Body>
                        <Image width={300} height={450} src="https://dummyimage.com/300x450"/>
                        <hr/>
                        <p>Price: <b>{dress.price}</b></p>
                    </Panel.Body>
                </Panel>
            </div>
        )
    }
}

export default ProductDetail
