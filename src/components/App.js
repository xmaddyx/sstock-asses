import React, { Component } from 'react'
import { Route } from "react-router-dom"
import Header from './Header'
import ProductList from './ProductList'
import ProductDetail from './ProductDetail'

class App extends Component {
    render() {
        return (
            <div>
                <Header />
                <Route exact path="/" component={ProductList} />
                <Route path="/img/:id" component={ProductDetail} dress={"wer"} />
            </div>
        )
    }
}

export default App
